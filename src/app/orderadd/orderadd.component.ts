import {  Component, OnInit,AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { OrderService } from '../order.service'; 
import { ProductService } from '../product.service'; 
import { CustomerService } from '../customer.service';  
declare var $;
@Component({
  selector: 'app-orderadd',
  templateUrl: './orderadd.component.html',
  styleUrls: ['./orderadd.component.css']
})
export class OrderaddComponent implements OnInit,AfterViewInit {
  
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private productService: ProductService,
    private custService: CustomerService,
    private datePipe: DatePipe,
    ) { }
reqDate:string;
temp=[];
customers=[];
product=[];
custcode:string;
orderref:string="";
remark:string="";
total:number;
productid;
qty:number;
orderdate=this.datePipe.transform(new Date(), 'dd/MM/yyyy');
userid:number;
text_alert:string;
orderno:string;
text_alertQty:string;
  ngOnInit() {
    this.userid=Number(localStorage.getItem('userid'));
    //this.reqDate=this.orderdate;
    this.productid="-1";
    // this.total=0;
    if(localStorage.getItem("OrderTemp")!==null){
      this.temp=JSON.parse(localStorage.getItem("OrderTemp"));
      //$('#dtOrder').DataTable().destroy();
    }
    this.LoadCust();
    this.custcode="-1";
  }
  ngAfterViewInit(){
    
    this.LoadTable();
    $('#datereq').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: true,
      icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
          $(this).data("DateTimePicker").date();
    });
  }
  LoadProduct(){
    this.productService.loadProduct().subscribe(
      data => {
        if(data[0].Status=="Complete"){
           this.product=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadCust(){
    this.custService.loadCust().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.customers=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  showModal(){
    $('#exampleModal').modal('show');
    this.LoadProduct();
    this.qty=0;
    this.productid="-1";
    this.text_alert="";
    this.text_alertQty="";
  }
  onSave(){
    //  $('#dtOrder').DataTable().destroy();
    if(this.productid!="-1"){
        if(this.qty!=null &&this.qty.toString()!="0"){
          if(this.qty>0){

          
          let resultCheck = this.temp.filter(({ProductID}) => ProductID === Number(this.productid));
          if(resultCheck.length>0){
            this.text_alert="สินค้านี้มีในรายการแล้ว";
            this.text_alertQty="";
          }
          else{
              let result = this.product.filter(({ProductIX}) => ProductIX === Number(this.productid));
              let productadd={
                  ProductID:result[0].ProductIX,
                  ProductCode:result[0].ProductCode,
                  ProductName:result[0].ProductName,
                  Qty:this.qty,
                  GroupID:result[0].GroupID
              }
              this.temp.push(productadd);
              localStorage.setItem("OrderTemp",JSON.stringify(this.temp));
              this.total+=this.qty;
              console.log(JSON.stringify(this.temp));
              $('#exampleModal').modal('hide');
              //$('#dtOrder').DataTable().destroy();
              this.LoadTable();
          }
        }
        else{
          this.text_alertQty="กรุณากรอกจำนวนสินค้ามากกว่า 0";
          this.text_alert="";
        }
      }
      else{
          this.text_alertQty="กรุณากรอกจำนวนสินค้า";
          this.text_alert="";
      }
    }
    else{
      if(this.productid=="-1"){
        this.text_alert="คุณยังไม่ได้เลือกสินค้า";
      }
    }
  }
  
  orderCancel(){
    //$('#dtOrder').DataTable().destroy();
    localStorage.removeItem("OrderTemp");
    this.temp=[];
    this.LoadTable();
  }

  addOrder(){
    this.reqDate=$('#reqDate').val();
    if(this.custcode!="-1"){

    
    if(this.reqDate!=""){
      if(this.temp.length>0){
          let result={
            ReqDate:this.reqDate,
            OrderDate:this.orderdate,
            UserID:this.userid,
            CustCode:this.custcode,
            OrderRef:this.orderref,
            Remark:this.remark,
            Item:this.temp
          }
          this.orderService.AddOrder(result).subscribe(
            data => {
              if(data[0].Status=="Complete"){
                this.orderno=data[0].OrderNo;
                this.text_alert="ใบสั่งซื้อหมายเลข "+ data[0].OrderNo +" ถูกสร้างเรียบร้อยแล้ว";
                $('#modalShow').modal('show');
                this.orderCancel();
              }
            },
            err => {
              console.log(err);
              alert(err);
            });
      }
      else{
        $('#modalShow2').modal('show');
      }
    }
    else{
      $('#modalShow1').modal('show');
    }
  }
    else{
      $('#modalShow3').modal('show');
    }
  }

  ok(){
    $('#modalShow').modal('hide');
    //window.location.href="/order";
    this.router.navigate(['order']);
  }

  cancel(){
    //window.location.href="/order";
    this.router.navigate(['order']);
  }
  delOrderItem(){
    let productItem=[];
    var result = $("tr:has(:checked)");
    var json = result.map(function () {
      return [$(this).children().slice(1).map(function () {
          return $(this).text().trim()
      }).get()]
    }).get();
    for(let i=0;i<json.length;i++){
        let productcode={
            "productcode":json[i][1]
        }
        productItem.push(productcode);
    }
    if(productItem.length>0){
        for(let i=0;i<productItem.length;i++){
            // let resultProduct = this.temp.filter(({ProductCode}) => ProductCode !== productItem[i].ProductCode);
            // for(let j=0;j<resultProduct.length;j++){
            //   let productadd={
            //       ProductID:resultProduct[j].ProductID,
            //       ProductCode:resultProduct[j].ProductCode,
            //       ProductName:resultProduct[j].ProductName,
            //       Qty:resultProduct[j].Qty,
            //       GroupID:resultProduct[j].GroupID
            //   }
            //   temp.push(productadd);
            // }
            this.temp.splice(this.temp.findIndex(e => e.ProductCode === productItem[i].productcode),1);
            localStorage.setItem("OrderTemp",JSON.stringify(this.temp));
            
        }
        var checkboxall=$('#example-select-all');
        if(checkboxall[0].checked){
          checkboxall[0].checked=false;
        }
        this.LoadTable();
    }
  }
  LoadTable(){
    $('#example').DataTable().destroy();
    setTimeout(function () {
      $(function () {	  
            var table = $('#example').DataTable({
              'columnDefs': [{
              'targets': 0,
              'searchable':false,
              'orderable':false,
              //'className': 'dt-body-center',
              'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' 
                  + $('<div/>').text(data).html() + '">';
              }
              }],
              'order': [1, 'asc'],
              "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'number' ?i : 0;
                    };

                    // Total over all pages
                    var total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                          return intVal(Number(a)) + intVal(Number(b));
                        }, 0 );

                    // Total over this page
                    var pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(Number(a)) + intVal(Number(b));
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                      pageTotal +' จากทั้งหมด ('+ total +')'
                    );
                }
            });

            // Handle click on "Select all" control
            $('#example-select-all').on('click', function(){
              // Check/uncheck all checkboxes in the table
              var rows = table.rows({ 'search': 'applied' }).nodes();
              $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });

            // Handle click on checkbox to set state of "Select all" control
            $('#example tbody').on('change', 'input[type="checkbox"]', function(){
              // If checkbox is not checked
              if(!this.checked){
              var el = $('#example-select-all').get(0);
              // If "Select all" control is checked and has 'indeterminate' property
              if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control 
                // as 'indeterminate'
                el.indeterminate = true;
              }
              }
            });
          });
        }, 2000);
  }
//  LoadTable(){
//   $('#dtOrder').DataTable().destroy();
//   setTimeout(function () {
//     $(function () {
//     $('#dtOrder').DataTable({
//         "footerCallback": function ( row, data, start, end, display ) {
//           var api = this.api(), data;
//           // Remove the formatting to get integer data for summation
//           var intVal = function ( i ) {
//               return typeof i === 'number' ?i : 0;
//           };
//           // Total over all pages
//           var total = api
//               .column( 3 )
//               .data()
//               .reduce( function (a, b) {
//                   return intVal(Number(a)) + intVal(Number(b));
//               }, 0 );
//               console.log('api='+api);
//           // Total over this page
//           var pageTotal = api
//               .column( 3, { page: 'current'} )
//               .data()
//               .reduce( function (a, b) {
//                 return intVal(Number(a)) + intVal(Number(b));
//               }, 0 );
//             console.log(pageTotal);
//           // Update footer
//           $( api.column( 3 ).footer() ).html(
//               pageTotal +' จากทั้งหมด ('+ total +')'
//           );
//     }
//   });
//     });
// }, 1000);
   
//  }
}
