import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  options;
  constructor(private http:Http) { 
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'bearer '+ localStorage.getItem('token')}); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); 
    this.options=options;
  }
   httpOrder:string=`${environment.apiUrl}/Order`;
   
  loadOrderItem(OrderIndex): Observable<any[]> {
    return this.http.get(`${this.httpOrder}/listitem/${OrderIndex}`,this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadOrderItemEdit(OrderIndex): Observable<any[]> {
    return this.http.get(`${this.httpOrder}/listitemedit/${OrderIndex}`,this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  loadOrder(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${this.httpOrder}/list`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json();
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  searchOrder(orderno): Observable<any> {
    return this.http.get(`${this.httpOrder}/search/${orderno}`,this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  CancelItem(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${this.httpOrder}/CancelItem`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json();
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  CancelOrder(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${this.httpOrder}/cancel`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json();
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  AddOrder(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${this.httpOrder}/add`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json();
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
}
