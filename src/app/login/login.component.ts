import { Component, OnInit, ViewChild } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { NgForm, FormControl } from '@angular/forms';
import { LoginService } from '../login.service';
import{Login} from '../class/login';
import {Md5} from 'ts-md5/dist/md5';
import { PermissionService } from '../permission.service'; 
declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})
export class LoginComponent implements OnInit {
  login:Login; 
  constructor(private router:Router,
    private activatedRoute: ActivatedRoute,
    private loginService: LoginService,
    private permService: PermissionService ) {
      this.login=new Login();
    
  }
  @ViewChild('loginForm') loginForm: NgForm;
  md5 = new Md5();
  ngOnInit() {
  }
textalert:string;
  doLogin(){
    if(this.login.username==""||this.login.username==undefined){
      this.textalert="คุณยังไม่ได้กรอกชื่อผู้ใช้";
      $('#myModal').modal('show');
   }
   else if(this.login.password==""||this.login.password==undefined){
    this.textalert="คุณยังไม่ได้รหัสผ่าน";
    $('#myModal').modal('show');
   }
    else{
      this.md5 = new Md5();
      //this.user_Class.Password=this.password;
      let password=this.md5.appendStr(this.login.password).end().toString();
	    this.login.password=password;
      this.loginService.doLogin(this.login).subscribe((data) => {
        if(data[0].Status=="Complete") {
            let user=data[0].Data;
            localStorage.setItem('token', user[0].token);
            localStorage.setItem('userid', user[0].UserID);
            localStorage.setItem('username', user[0].Username);
            localStorage.setItem('permisID', user[0].PermisID);
            localStorage.setItem('fullname', user[0].FullName);
            this.LoadMenuConfig(user[0].PermisID,user[0].token);
            //this.router.navigate(['','main']);
            //window.location.href="/main";
        } 
        else {
           this.textalert="ชื่อผู้ใช้หรือรหัสผ่านผิด";
           $('#myModal').modal('show');
      }
      });
    }
  }
  LoadMenuConfig(permid:string,token:string){
    this.permService.CheckMenuConfig(permid,token).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          if(data[0].Data.length>0){
            window.location.href="/main";
          }else{
            this.textalert="ไม่สามารถเข้าสู่ระบบได้เนื่องจากสิทธิ์การใช้งานถูกปิด";
            $('#myModal').modal('show');
          }
        }
      },
        err => {
          console.log(err);
          alert(err);
        });
    }

}
