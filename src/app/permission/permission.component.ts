import { Component, OnInit,AfterViewInit  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionService } from '../permission.service';  
import{Permission_class} from '../class/permission_class';
import { FactoryService } from '../factory.service';  
declare var $;
@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.css'],
  providers:[PermissionService]
})
export class PermissionComponent implements OnInit,AfterViewInit {
  

  Permis_Class:Permission_class;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
		private permService: PermissionService,
		private facService: FactoryService) {
      this.Permis_Class=new Permission_class();
      
     }
   
    mode="";
    Permission=[];
	Menu=[];
	MenuConfig=[];
    permisid:string;
    permisname:string;
    status:string;
    titlePopup;
	menucheck=[];
	menumap=[];
	txtAlert:string;
	text_alert:string;
	txt_alertname:string;
	factory=[];
	factorycode:string;
	text_alertFactory:string;
	

  ngOnInit():void {
	if(localStorage.getItem("m_permission")== "false"){
		window.location.href="/main";
	}
    this.LoadPermis();
		this.LoadMenu();
		this.LoadFactory();
	}
	ngAfterViewInit(){
		this.LoadTable();
	}
  
  onSave(){
	this.txt_alertname="";
	  this.text_alertFactory="";
    if(this.permisname.trim()!=""&&this.factorycode!="-1"&&this.factorycode!=""){

		this.Permis_Class.PermisName=this.permisname;
		this.Permis_Class.UserBy=Number('1002');
		this.Permis_Class.FactoryCode=this.factorycode;
		if(this.status=="1"){
		  this.Permis_Class.IsActive=true;
		}else{
		  this.Permis_Class.IsActive=false;
		}
		if(this.mode=="add"){
			this.permService.addPermission(this.Permis_Class).subscribe(
			  data => { 
				if(data[0].Status=="Complete"){
					this.Permis_Class.PermisID=Number(data[0].Data[0].PermissionID);
					this.Permis_Class.IsUsed=true;
					for (var x = 0; x < this.menucheck.length; x++) {
						this.Permis_Class.MenuID=this.menucheck[x];
						this.permService.editMenuConfig(this.Permis_Class).subscribe(
						  data => { 
							if(data[0].Status=="Complete"){
							}
						  },
						  err => {
							alert(err);
							console.log(err);
						  });
					}
					this.txtAlert="เพิ่มสิทธิ์การใช้งานเรียบร้อยแล้ว";
					this.LoadPermis();
					$('#modalShow').modal('show');
				}
			  },
			  err => {
				console.log(err);
			  });
		  }
		  else{
			this.Permis_Class.PermisID=Number(this.permisid);
			this.permService.editPermission(this.Permis_Class).subscribe(
			  data => { 
				if(data[0].Status=="Complete"){
					this.Permis_Class.IsUsed=true;
					for (var x = 0; x < this.menucheck.length; x++) {
						this.Permis_Class.MenuID=this.menucheck[x];
						this.permService.editMenuConfig(this.Permis_Class).subscribe(
						  data => { 
							if(data[0].Status=="Complete"){
							}
						  },
						  err => {
							alert(err);
							console.log(err);
						  });
					}
					this.txtAlert="แก้ไขสิทธิ์การใช้งานเรียบร้อยแล้ว";
					this.LoadPermis();
					$('#modalShow').modal('show');
					
				}
			  },
			  err => {
				console.log(err);
			  });
		  }
		this.LoadTable();
		$('#exampleModal').modal('hide');
    }
	if(this.permisname.trim()==""){
      this.txt_alertname="โปรดกรอกชื่อสิทธิ์การใช้งาน";
    }else{
      this.txt_alertname="";
		}
		if(this.factorycode=="-1"||this.factorycode==""){
			this.text_alertFactory="กรุณาเลือกโรงงาน";
		}
		else{
			this.text_alertFactory="";
		}
  }
  addPermis(){
      this.mode="add";
      this.permisid;
      this.permisname="";
      this.status="1";
      this.titlePopup="เพิ่มสิทธิ์การใช้งาน"
	  this.menucheck=[];
	  this.menumap=[];
		this.MenuConfig=[];	
		this.factorycode="-1";
		this.txt_alertname="";
		this.text_alertFactory="";
      console.log(this.mode);
  }
  onEditbtnClick(permisid,permisname,factorycode,isactive){
      this.titlePopup="แก้ไขสิทธิ์การใช้งาน"
      this.mode="edit";
      this.permisid=permisid;
			this.permisname=permisname;
			this.factorycode=factorycode;
	  this.menucheck=[];
	  this.menumap=[];
	  this.MenuConfig=[];
	  this.text_alert="";
		this.txt_alertname="";
		this.text_alertFactory="";
      if(isactive){
        this.status="1";
      }else{
        this.status="0";
      }
	  this.LoadMenuConfig(this.permisid);
  }
  LoadPermis(){
    this.permService.loadPermission().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.Permission=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  
  LoadMenu(){
    this.permService.loadMenu().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.Menu=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
	}
	LoadFactory(){
    this.facService.loadFactory().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.factory=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  
  LoadMenuConfig(permid:string){
    this.permService.loadMenuConfig(permid).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.MenuConfig=data[0].Data;
		  //alert(JSON.stringify(this.MenuConfig));
		  //alert(this.MenuConfig.length);
		  this.updateCheckedMenu();
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  
  updateCheckedMenu() {
	for (var x = 0; x < this.MenuConfig.length; x++) {
        this.menucheck.push(this.MenuConfig[x].MenuID);
    }
	/*alert(JSON.stringify(this.MenuConfig));
	for (var x = 0; x < this.MenuConfig.length; x++) {
		alert(this.MenuConfig[x].MenuID);
    }*/
	//alert(this.menucheck);
}
  updateCheckedOptions(item, event ,i) {
   //this.optionsMap[option] = event.target.checked;
   //this.menucheck.push('4');
	if(this.menucheck.indexOf(i) >= 0){
		this.menucheck.splice(this.menucheck.indexOf(i),1);
	}
	else{
		this.menucheck.push(i);
	}
	//alert(this.menucheck);
}
	LoadTable(){
    $('#datable1').DataTable().destroy();
    setTimeout(function () {
        $(function () {
      $('#datable1').DataTable({
				"columnDefs": [
          { "orderable": false,
            "targets": 3
          }
        ],"order": []
			});
        });
    }, 2000);
  }
 

}
