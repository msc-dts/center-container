import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  options;
  constructor(private http:Http) { 
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'bearer '+ localStorage.getItem('token')}); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); 
    this.options=options;
  }

  loadCust(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Customer/list`,this.options)
      .map((res: Response) => {
        return   res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  addCust(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${environment.apiUrl}/Customer/AddCust`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  editCust(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${environment.apiUrl}/Customer/UpdateCust`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  CheckDup(CustCode): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Customer/checkdup/${CustCode}`,this.options)
      .map((res: Response) => {
        return   res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
