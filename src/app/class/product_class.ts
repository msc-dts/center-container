export class Product_class {
    ProductIX:number;
    ProductCode:string;
    ProductName:string;
    GroupID:number;
    IsActive:boolean;
    UserID:number;
    FactoryCode:string;
}