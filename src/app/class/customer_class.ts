export class Customer_Class {
    CustIndex:number;
    CustCode:string;
    CustName:string;
    CustAddr:string;
    CustTel:string;
    IsActive:boolean;
    UserID:number;
}
