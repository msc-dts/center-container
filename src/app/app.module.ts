import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DatePipe} from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductgroupComponent } from './productgroup/productgroup.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ProductComponent } from './product/product.component';
import { OrderComponent } from './order/order.component';
import { UserComponent } from './user/user.component';
import { PermissionComponent } from './permission/permission.component';
import { LoginComponent } from './login/login.component';
import { OrderaddComponent } from './orderadd/orderadd.component';
import { OrdereditComponent } from './orderedit/orderedit.component';
import { TableproductionComponent } from './tableproduction/tableproduction.component';
import { CustomerComponent } from './customer/customer.component';

export function HttpLoaderFactory(http: HttpClient) {
}
@NgModule({
  declarations: [
    AppComponent,
    ProductgroupComponent,
    HomeComponent,
    MainComponent,
    ProductComponent,
    OrderComponent,
    LoginComponent,
	UserComponent,
	PermissionComponent,
    OrderaddComponent,
    OrdereditComponent,
    TableproductionComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
