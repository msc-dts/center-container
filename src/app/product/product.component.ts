import { Component, OnInit,AfterViewInit  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductgroupService } from '../productgroup.service';  
import { ProductService } from '../product.service'; 
import{Product_class} from '../class/product_class';
import { FactoryService } from '../factory.service';  
declare var $;
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit,AfterViewInit {
  product_Class:Product_class;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private prodgrdService: ProductgroupService,
    private productService: ProductService,
    private facService: FactoryService) {
      this.product_Class=new Product_class();
      
     }
     mode="";
     productgroup=[];
     product=[];
     prodid:string;
     prodcode:string;
     prodname:string;
     groupid:string;
     capacity:string;
     status:string;
     titlePopup;
     text_alert:string;
     txt_alertcode:string;
     txt_alertname:string;
     txtAlert:string;
     isEdit:boolean;
     factory=[];
	factorycode:string;
	text_alertFactory:string;
  ngOnInit() {
	if(localStorage.getItem("m_product")== "false"){
		window.location.href="/main";
	}
    this.LoadProduct();
    this.LoadProdGroup();
    this.LoadFactory();
  }
  ngAfterViewInit(){
    this.LoadTable();
  }
  LoadProdGroup(){
    this.prodgrdService.loadProductGroup().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.productgroup=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadProduct(){
    this.productService.loadProduct().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.product=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadFactory(){
    this.facService.loadFactory().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.factory=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  onSave(){
    if(this.prodcode!="" && this.prodname!=""&&this.groupid!="-1"&&this.factorycode!="-1"){
      this.text_alert="";
      this.txt_alertcode="";
      this.txt_alertname="";
    this.product_Class.ProductCode=this.prodcode;
    this.product_Class.ProductName=this.prodname;
    this.product_Class.GroupID=Number(this.groupid)
    this.product_Class.FactoryCode=this.factorycode;
    this.product_Class.UserID=Number(localStorage.getItem("userid"));
    if(this.status=="1"){
      this.product_Class.IsActive=true;
    }else{
      this.product_Class.IsActive=false;
    }
    if(this.mode=="add"){
        this.CheckProdCodeDup(this.prodcode);
      }
      else{
        this.product_Class.ProductIX=Number(this.prodid);
        this.productService.editProduct(this.product_Class).subscribe(
          data => { 
            if(data[0].Status=="Complete"){
                this.LoadProduct();
                this.txtAlert="แก้ไขข้อมูลเรียบร้อยแล้ว";
                // $('#exampleModal').modal('hide');
                $('#dtProduct').DataTable().destroy();
                this.LoadTable();
                $('#modalShow').modal('show');
            }
          },
          err => {
            console.log(err);
          });
      }
    }

    if(this.groupid=="-1"){
        this.text_alert="โปรดเลือกกลุ่มสินค้า";
    }
    else{
      this.text_alert="";
    }
    if(this.factorycode=="-1"){
      this.text_alertFactory="โปรดเลือกโรงงาน";
    }
    else{
      this.text_alertFactory="";
    }
    if(this.prodcode==""){
      this.txt_alertcode="โปรดกรอกรหัสสินค้า";
    }else{
      this.txt_alertcode="";
    }
    if(this.prodname==""){
      this.txt_alertname="โปรดกรอกชื่อสินค้า";
    }else{
      this.txt_alertname="";
    }
  }
  addProduct(){
      this.mode="add";
      this.isEdit=false;
      this.prodid="";
      this.prodcode="";
      this.prodname="";
      this.groupid="-1";
      this.factorycode="-1";
      this.status="1";
      this.titlePopup="เพิ่มสินค้า"
      this.text_alert="";
      this.txt_alertcode="";
      this.txt_alertname="";
      this.text_alertFactory="";
      //console.log(this.mode);
  }
  //item.ProductIX,item.ProductCode,item.ProductName,item.IsActive,item.GroupID
  onEditbtnClick(productIX,productcode,productname,isactive,groupid,factorycode){
      this.titlePopup="แก้ไขสินค้า"
      this.isEdit=true;
      this.mode="edit";
      this.prodid=productIX;
      this.prodcode=productcode;
      this.prodname=productname;
      this.groupid=groupid;
      this.factorycode=factorycode;
      this.text_alert="";
      this.text_alertFactory="";
      if(isactive){
        this.status="1";
      }else{
        this.status="0";
      }
  }
   CheckProdCodeDup(productcode:string){
    this.productService.checkDupCode(productcode).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          if(data[0].Num==0){
            this.productService.addProduct(this.product_Class).subscribe(
              data => { 
                if(data[0].Status=="Complete"){
                    this.LoadProduct();
                    
                    $('#dtProduct').DataTable().destroy();
                    this.LoadTable();
                    this.txtAlert="เพิ่มข้อมูลเรียบร้อยแล้ว";
                    $('#modalShow').modal('show');
                }
              },
              err => {
                console.log(err);
              });
          }
          else{
            this.txt_alertcode="รหัสสินค้านี้มีอยู่ในระบบแล้ว";
            
          }
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadTable(){
    $('#exampleModal').modal('hide');
    setTimeout(function () {
        $(function () {
      $('#dtProduct').DataTable({
        "columnDefs": [
          { "orderable": false,
            "targets": 5
          }
        ],"order": []
      });
        });
    }, 2000);
  }
 

}
