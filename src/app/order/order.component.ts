import { Component, OnInit,AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { OrderService } from '../order.service'; 
declare var $;
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit,AfterViewInit {

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private datePipe: DatePipe) { }
order=[];
orderitem=[];
startdate;
enddate;
searchdate:string;
text_confirmcancel:string;
ordernocancel:string;
orderno:string;
orderindex:number;
reqDate:string;
total:string;
orderindexcancel:number;
  ngOnInit() {
	if(localStorage.getItem("m_order")== "false"){
		window.location.href="/main";
	}

    let todate=new Date();
    this.startdate=this.datePipe.transform(todate.setMonth(todate.getMonth()-1), 'dd/MM/yyyy');
    this.enddate=this.datePipe.transform(new Date(), 'dd/MM/yyyy');
    this.LoadOrder();
  }
  ngAfterViewInit(){
    this.LoadTable();
    $('#datetime1').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: true,
      icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
          $(this).data("DateTimePicker").date();
    }).on('dp.change', function (e) { });
    $('#datetime2').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: true,
      icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
    }).on('dp.show', function() {
        if($(this).data("DateTimePicker").date() === null)
          $(this).data("DateTimePicker").date();
    }).on('dp.change', function (e) { });
  }
  LoadOrder(){
    var dt1 = this.startdate.split('/'),
            dt2 = this.enddate.split('/'),
            one = new Date(dt1[2], dt1[1]-1, dt1[0]),
            two = new Date(dt2[2], dt2[1]-1, dt2[0]);

        var millisecondsPerDay = 1000 * 60 * 60 * 24;
        var millisBetween = two.getTime() - one.getTime();
        var days = millisBetween / millisecondsPerDay;
        console.log(days);
    if(days>=0){
      let search={
        StartDate:this.startdate,
        EndDate:this.enddate
      }
      this.orderService.loadOrder(search).subscribe(
        data => {
          if(data[0].Status=="Complete"){
            this.order=data[0].Data;
          }
        },
        err => {
          console.log(err);
          alert(err);
      });
    }else{
      $('#modalAlert').modal('show');
    }
  }
 
  search(){
    this.startdate=$('#datestart').val();
    this.enddate=$('#dateend').val();
    this.LoadOrder();
    //$('#dtOrder1').DataTable().destroy();
    this.LoadTable();
  }
  cancelOrder(orderno,orderindex){
    this.text_confirmcancel="คุณต้องการยกเลิกใบสั่งซื้อหมายเลข "+orderno+" หรือไม่";
    $('#modalConfirm').modal('show');
    this.ordernocancel=orderno;
    this.orderindexcancel=orderindex;
  }
  showCancel(orderindex,orderno,reqdate){
    this.orderno=orderno;
    this.orderindex=orderindex;
    this.reqDate=reqdate;
    this.LoadOrderCancel();
  }
  LoadOrderCancel(){
    this.orderService.loadOrderItem(this.orderindex).subscribe(
      data => {
        if(data[0].Status=="Complete"&&data[0].OrderNo!=""){
          this.orderitem=data[0].Data;
          this.total=data[0].Total;
          $('#modalCancel').modal('show');
          setTimeout(function () {
            $(function () {
                $('#dtOrderItemCancel').DataTable();
            });
          }, 2000);
          
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  confirmCancel(){
    $('#modalConfirm').modal('hide');
    let data={
      OrderNo:this.ordernocancel,
      OrderIndex:this.orderindexcancel,
      UserID:Number(localStorage.getItem("userid"))
    }
    this.orderService.CancelOrder(data).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.LoadOrder();
          this.LoadTable();
          $('#modalShow').modal('hide');
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  edit(orderno,orderindex,reqdate){
    this.router.navigate(['orderedit',orderindex]);
    //window.location.href="/orderedit/"+orderno;
    
  }
  newOrder(){
    //window.location.href="/orderadd";
    this.router.navigate(['orderadd']);
  }
  LoadTable(){
    $('#dtOrder1').DataTable().destroy();
      setTimeout(function () {
        $(function () {
          $('#dtOrder1').DataTable({
            "columnDefs": [
              { "orderable": false, "targets": 4 }
            ],"order": []
          });
        });
      }, 1000);
     
   }
   
}
