import { TestBed } from '@angular/core/testing';

import { TableproductionService } from './tableproduction.service';

describe('TableproductionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableproductionService = TestBed.get(TableproductionService);
    expect(service).toBeTruthy();
  });
});
