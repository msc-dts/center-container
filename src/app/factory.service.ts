import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FactoryService {

  options;
  constructor(private http:Http) { 
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'bearer '+ localStorage.getItem('token')}); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); 
    this.options=options;
  }

  loadFactory(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Factory/list`,this.options)
      .map((res: Response) => {
        return   res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
