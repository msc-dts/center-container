import { Component, OnInit,AfterViewInit  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductgroupService } from '../productgroup.service';  
import{ProductGroup_class} from '../class/productgroup_class';

declare var $;
@Component({
  selector: 'app-productgroup',
  templateUrl: './productgroup.component.html',
  styleUrls: ['./productgroup.component.css'],
  providers:[ProductgroupService]
})


export class ProductgroupComponent implements OnInit,AfterViewInit {
  

  PrdGroup_Class:ProductGroup_class;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private prodgrdService: ProductgroupService) {
      this.PrdGroup_Class=new ProductGroup_class();
      
     }
   
    mode="";
    productgroup=[];
    prodid:string;
    prodname:string;
    capacity:string;
    status:string;
    titlePopup;
    txtAlert:string;
    txt_alert:string;
    txt_alertname:string;
    prodgroupname:string;
  ngOnInit():void {
	if(localStorage.getItem("m_productgroup")== "false"){
		window.location.href="/main";
	}
    this.LoadProdGroup();
  }
  ngAfterViewInit(){
    this.LoadTable();
  }
  
  onSave(){
    if(this.prodname!="" && this.capacity!=""){
      if(Number(this.capacity)>0){
      this.PrdGroup_Class.GroupName=this.prodname;
      this.PrdGroup_Class.Capacity=Number(this.capacity);
      this.PrdGroup_Class.UserID=Number(localStorage.getItem("userid"));
        if(this.status=="1"){
          this.PrdGroup_Class.IsActive=true;
        }else{
          this.PrdGroup_Class.IsActive=false;
        }
        if(this.mode=="add"){
            this.SearchProdGroup();
        }
        else{
            if(this.prodname==this.prodgroupname){
              this.UpdateProductGroup();
            }
            else{
              this.SearchProdGroup();
            }
        }
      }
      else{
        this.txt_alert="กรุณากรอกจำนวนการผลิตต่อวันมากกว่า 0";
      }
      
    }
  }
  addGroup(){
      this.mode="add";
      this.prodid;
      this.prodname="";
      this.capacity="";
      this.status="1";
      this.titlePopup="เพิ่มกลุ่มสินค้า";
      this.txt_alert="";
      this.txt_alertname="";
      console.log(this.mode);
  }
  onEditbtnClick(groupid,groupname,capacity,isactive){
      this.titlePopup="แก้ไขกลุ่มสินค้า"
      this.mode="edit";
      this.prodid=groupid;
      this.prodname=groupname;
      this.prodgroupname=groupname;
      this.capacity=capacity;
      this.txt_alertname="";
      this.txt_alert="";
      if(isactive){
        this.status="1";
      }else{
        this.status="0";
      }
  }
  LoadProdGroup(){
    this.prodgrdService.loadProductGroup().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.productgroup=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  SaveProductGroup(){
    this.prodgrdService.addProductGroup(this.PrdGroup_Class).subscribe(
      data => { 
        if(data[0].Status=="Complete"){
            this.LoadProdGroup();
            this.txtAlert="เพิ่มข้อมูลเรียบร้อยแล้ว";
            $('#exampleModal').modal('hide');
            //$('#datable1').DataTable().destroy();
            this.LoadTable();
            $('#modalShow').modal('show');
        }
      },
      err => {
        console.log(err);
      });
  }
  UpdateProductGroup(){
    this.PrdGroup_Class.GroupID=Number(this.prodid);
          this.prodgrdService.editProductGroup(this.PrdGroup_Class).subscribe(
            data => { 
              if(data[0].Status=="Complete"){
                  this.LoadProdGroup();
                  this.txtAlert="อัพเดทข้อมูลเรียบร้อยแล้ว";
                  $('#exampleModal').modal('hide');
                  this.LoadTable();
                  $('#modalShow').modal('show');
              }
            },
            err => {
              console.log(err);
            });
  }
  SearchProdGroup(){
    this.prodgrdService.searchProductGroup(this.prodname).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          if(data[0].Data.length==0){
            if(this.mode=="add"){
                this.SaveProductGroup();
            }
            else{
                this.UpdateProductGroup();
            }
          }
          else{
            this.txt_alertname="ชื่อกลุ่มสินค้านี้มีอยู่ในระบบแล้ว";
          }
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadTable(){
    $('#datable1').DataTable().destroy();
    setTimeout(function () {
        $(function () {
      $('#datable1').DataTable({
        "columnDefs": [
          { "orderable": false, "targets": 3 }
        ],"order": []
      });
        });
    }, 2000);
  }
 

}
