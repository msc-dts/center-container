import { Component, OnInit,AfterViewInit  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../customer.service';  
import{Customer_Class} from '../class/customer_class';
declare var $;
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit,AfterViewInit {
  Cust_Class:Customer_Class;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private custService: CustomerService) {
      this.Cust_Class=new Customer_Class();
      
     }
     mode="";
    customers=[];
    custindex:number;
    custcode:string;
    custname:string;
    custaddr:string;
    custtel:string;
    status:string;
    titlePopup;
    txtAlert:string;
    txt_alertcode:string;
    txt_alertname:string;
    isEdit:boolean;
  ngOnInit() {
    this.LoadCust();
  }
  ngAfterViewInit(){
    this.LoadTable();
  }

  onSave(){
    if(this.custcode!="" && this.custname!=""){
      this.Cust_Class.CustCode=this.custcode;
      this.Cust_Class.CustName=this.custname;
      this.Cust_Class.CustAddr=this.custaddr;
      this.Cust_Class.CustTel=this.custtel;
      this.Cust_Class.UserID=Number(localStorage.getItem("userid"));
        if(this.status=="1"){
          this.Cust_Class.IsActive=true;
        }else{
          this.Cust_Class.IsActive=false;
        }
        if(this.mode=="add"){
            this.CheckCustCode();
        }
        else{
            this.UpdateCust();
        }
     }
     if(this.custcode==""){
      this.txt_alertcode="กรุณากรอกรหัสลูกค้า";
     }
     if(this.custname==""){
      this.txt_alertcode="กรุณากรอกชื่อลูกค้า";
     }
      
    
  }
  addCustomer(){
      this.mode="add";
      this.custindex;
      this.custname="";
      this.custcode="";
      this.custaddr="";
      this.custtel="";
      this.status="1";
      this.titlePopup="เพิ่มข้อมูลลูกค้า";
      this.txt_alertcode="";
      this.txt_alertname="";
      this.isEdit=false;
      // console.log(this.mode);
  }
  onEditbtnClick(custindex,custcode,custname,custaddr,custtel,isactive){
      this.titlePopup="แก้ไขข้อมูลลูกค้า"
      this.mode="edit";
      this.isEdit=true;
      this.custindex=custindex;
      this.custcode=custcode;
      this.custname=custname;
      this.custaddr=custaddr;
      this.custtel=custtel;
      this.txt_alertname="";
      this.txt_alertcode="";
      if(isactive){
        this.status="1";
      }else{
        this.status="0";
      }
  }
  LoadCust(){
    this.custService.loadCust().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.customers=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  SaveCust(){
    this.custService.addCust(this.Cust_Class).subscribe(
      data => { 
        if(data[0].Status=="Complete"){
            this.LoadCust();
            this.txtAlert="เพิ่มข้อมูลเรียบร้อยแล้ว";
            $('#exampleModal').modal('hide');
            this.LoadTable();
            $('#modalShow').modal('show');
        }
      },
      err => {
        console.log(err);
      });
  }
  UpdateCust(){
    this.Cust_Class.CustIndex=Number(this.custindex);
          this.custService.editCust(this.Cust_Class).subscribe(
            data => { 
              if(data[0].Status=="Complete"){
                  this.LoadCust();
                  this.txtAlert="อัพเดทข้อมูลเรียบร้อยแล้ว";
                  $('#exampleModal').modal('hide');
                  this.LoadTable();
                  $('#modalShow').modal('show');
              }
            },
            err => {
              console.log(err);
            });
  }
  CheckCustCode(){
    this.custService.CheckDup(this.custcode).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          if(data[0].Data[0].Num==0){
            this.SaveCust();
          }
          else{
            this.txt_alertcode="รหัสสินค้านี้มีอยู่ในระบบแล้ว";
            
          }
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  
  LoadTable(){
    $('#datable1').DataTable().destroy();
    setTimeout(function () {
        $(function () {
      $('#datable1').DataTable({
        "columnDefs": [
          { "orderable": false, "targets": 4 }
        ],"order": []
      });
        });
    }, 2000);
  }
}
