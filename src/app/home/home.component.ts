import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionService } from '../permission.service'; 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,private permService: PermissionService) {
  }
	fullname:string;
	m_order=false;
	m_productiongroup=false;
	m_product=false;
	m_productgroup=false;
	m_user=false;
	m_permission=false;	
	m_customer=false;	
	MenuConfig=[];
  ngOnInit() {
    this.fullname=localStorage.getItem("fullname");
	this.LoadMenuConfig(localStorage.getItem("permisID"));
  }
logout(){
  localStorage.removeItem("token");
  localStorage.removeItem("username");
  localStorage.removeItem("permisID");
  localStorage.removeItem("fullname");
  //this.router.navigate(['/login']);
  window.location.href="/login";
}
	LoadMenuConfig(permid:string){
    this.permService.loadMenuConfig(permid).subscribe(
      data => {
        if(data[0].Status=="Complete"){
			this.MenuConfig=data[0].Data;
			//alert(JSON.stringify(this.MenuConfig[0]));
			localStorage.setItem("m_order", this.m_order.toString());
			localStorage.setItem("m_productiongroup", this.m_productiongroup.toString());
			localStorage.setItem("m_product", this.m_product.toString());
			localStorage.setItem("m_productgroup", this.m_productgroup.toString());
			localStorage.setItem("m_user", this.m_user.toString());
			localStorage.setItem("m_permission", this.m_permission.toString());
			localStorage.setItem("m_customer", this.m_customer.toString());
			for(var i = 0; i < this.MenuConfig.length; i++){
				if(this.MenuConfig[i].MenuID==1){
					this.m_order=true;
					localStorage.setItem("m_order", this.m_order.toString());
					
				}
				else if(this.MenuConfig[i].MenuID==2){
					this.m_productgroup=true;
					localStorage.setItem("m_productgroup", this.m_productgroup.toString());
				}
				else if(this.MenuConfig[i].MenuID==3){
					this.m_productiongroup=true;
					localStorage.setItem("m_productiongroup", this.m_productiongroup.toString());
				}
				else if(this.MenuConfig[i].MenuID==4){
					this.m_user=true;
					localStorage.setItem("m_user", this.m_user.toString());
				}
				else if(this.MenuConfig[i].MenuID==5){
					this.m_product=true;
					localStorage.setItem("m_product", this.m_product.toString());
				}
				else if(this.MenuConfig[i].MenuID==6){
					this.m_permission=true;
					localStorage.setItem("m_permission", this.m_permission.toString());
				}
				else if(this.MenuConfig[i].MenuID==7){
					this.m_customer=true;
					localStorage.setItem("m_customer", this.m_customer.toString());
				}
			}
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
}
