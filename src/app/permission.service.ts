import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  options;
  constructor(private http:Http) { 
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'bearer '+ localStorage.getItem('token')}); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); 
    this.options=options;
  }

  loadPermission(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Permission/list`,this.options)
      .map((res: Response) => {
        return   res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  loadMenu(): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Menu/list`,this.options)
      .map((res: Response) => {
        return   res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  loadMenuConfig(PermisID): Observable<any[]> {
    return this.http.get(`${environment.apiUrl}/Menu/config/${PermisID}`,this.options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  CheckMenuConfig(PermisID,token): Observable<any[]> {
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'bearer '+token}); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); 
    return this.http.get(`${environment.apiUrl}/Menu/config/${PermisID}`,options)
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  addMenuConfig(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${environment.apiUrl}/Menu/CreateConfig`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  editMenuConfig(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${environment.apiUrl}/Menu/UpdateConfig`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  addPermission(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
	//alert(JSON.stringify(body));
    return this.http.post(`${environment.apiUrl}/Permission/Create`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }
  editPermission(body): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    return this.http.post(`${environment.apiUrl}/Permission/UpdatePermission`, bodyString, this.options) // ...using post request
      .map((res: Response) => {
        return res.json()
      }) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any 
  }

}
