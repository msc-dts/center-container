import { Component, OnInit,AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { TableproductionService } from '../tableproduction.service'; 
declare var $;

@Component({
  selector: 'app-tableproduction',
  templateUrl: './tableproduction.component.html',
  styleUrls: ['./tableproduction.component.css']
})
export class TableproductionComponent implements OnInit,AfterViewInit {

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private tableService: TableproductionService,
    private datePipe: DatePipe) { }
month:number;
groupid:number;
  ngOnInit() {
    let todate=new Date();
     this.month = todate.getMonth()+1;
     this.groupid=1;
     this.LoadData();
  }
  ngAfterViewInit(){
  }
  LoadData(){
    let data={
      GroupID:this.groupid,
      Month:this.month
    }
    this.tableService.getData(data).subscribe(
      data => {
        if(data!=[]){
          setTimeout(function () {
            $(function () {
                $('#calendar').fullCalendar({
                  events: data
                });
              });
          }, 2000);
          
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
}
