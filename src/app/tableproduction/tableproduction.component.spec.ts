import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableproductionComponent } from './tableproduction.component';

describe('TableproductionComponent', () => {
  let component: TableproductionComponent;
  let fixture: ComponentFixture<TableproductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableproductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableproductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
