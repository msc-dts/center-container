import { Component, OnInit,AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { OrderService } from '../order.service'; 
declare var $;
@Component({
  selector: 'app-orderedit',
  templateUrl: './orderedit.component.html',
  styleUrls: ['./orderedit.component.css']
})
export class OrdereditComponent implements OnInit,AfterViewInit {

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private orderService: OrderService,
    private datePipe: DatePipe) { }
orderno:string;
reqDate:string;
orderindex:number;
orderitem=[];
order=[];
total:number;
userid:number;
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params['orderindex']) {
          this.orderindex=params['orderindex'];
          this.userid=Number(localStorage.getItem("userid"));
          this.LoadOrder();
      }
    });
  }
  ngAfterViewInit(){
    this.LoadTable();
  }
  LoadOrderItem(){
    this.orderService.loadOrderItemEdit(this.orderindex).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.orderitem=data[0].Data;
          this.total=data[0].Total;
          
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadOrder(){
    this.orderService.searchOrder(this.orderindex).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.order=data[0].Data;
          this.reqDate=this.order[0].RequiredDate;
          this.orderindex=this.order[0].OrderIndex;
          this.orderno=this.order[0].OrderNo;
          //console.log(this.order+' '+this.reqDate);
          this.LoadOrderItem();
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  onUpdate(){
    var result = $("tr:has(:checked)");
    var json = result.map(function () {
      return [$(this).children().slice(1).map(function () {
          return $(this).text().trim()
      }).get()]
    }).get();
    if(json.length>0){
      $('#modalConfirm').modal('show');
    }
    else{
      $('#modalAlert').modal('show');
    }
  }
  cancel(){
    //window.location.href="/order";
    this.router.navigate(['order']);
  }
  orderCancel(){
    let orderItem=[];
    var result = $("tr:has(:checked)");
    var json = result.map(function () {
      return [$(this).children().slice(1).map(function () {
          return $(this).text().trim()
      }).get()]
    }).get();
    for(let i=0;i<json.length;i++){
        let orderindex={
            "ItemIndex":json[i][4]
        }
        orderItem.push(orderindex);
    }
    let Order={
      "OrderNo":this.orderno,
      "Item":orderItem,
      "UserID":this.userid
    }
    this.orderService.CancelItem(Order).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.LoadOrderItem();
          $('#modalShow').modal('show');

        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  confirmCancel(){
    this.orderCancel();
    $('#modalConfirm').modal('hide');
    //$('#example').DataTable().destroy();
    this.LoadTable();
  }

  LoadTable(){
    $('#example').DataTable().destroy();
    setTimeout(function () {
      $(function () {	  
            var table = $('#example').DataTable({
              'columnDefs': [{
              'targets': 0,
              'searchable':false,
              'orderable':false,
              //'className': 'dt-body-center',
              'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' 
                  + $('<div/>').text(data).html() + '">';
              }
              }],
              'order': [1, 'asc'],
              "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'number' ?i : 0;
                    };

                    // Total over all pages
                    var total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                          return intVal(Number(a)) + intVal(Number(b));
                        }, 0 );

                    // Total over this page
                    var pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(Number(a)) + intVal(Number(b));
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                      pageTotal +' จากทั้งหมด ('+ total +')'
                    );
                }
            });

            // Handle click on "Select all" control
            $('#example-select-all').on('click', function(){
              // Check/uncheck all checkboxes in the table
              var rows = table.rows({ 'search': 'applied' }).nodes();
              $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });

            // Handle click on checkbox to set state of "Select all" control
            $('#example tbody').on('change', 'input[type="checkbox"]', function(){
              // If checkbox is not checked
              if(!this.checked){
              var el = $('#example-select-all').get(0);
              // If "Select all" control is checked and has 'indeterminate' property
              if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control 
                // as 'indeterminate'
                el.indeterminate = true;
              }
              }
            });
          });
        }, 2000);
  }

}
