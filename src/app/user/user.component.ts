import { Component, OnInit  } from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionService } from '../permission.service';  
import { UserService } from '../user.service'; 
import{User_class} from '../class/user_class';
declare var $;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user_Class:User_class;
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private permisService: PermissionService,
    private userService: UserService) {
      this.user_Class=new User_class();
      
     }
     mode="";
	 button="";
     permis=[];
     user=[];
     userid:string;
     username:string;
     fullname:string;
	 password:string;
     permisid:string;
     status:string;
     titlePopup;
     text_alert:string;
     txt_alertcode:string;
     txt_alertname:string;
	 txt_alertpw:string;
	 txtAlert:string;
   isEdit:boolean;
   old_pass:string;
	 md5 = new Md5();
  ngOnInit() {
	if(localStorage.getItem("m_user")== "false"){
		window.location.href="/main";
	}
    this.LoadUser();
    this.LoadPermis();
  }
  
  ngAfterViewInit(){
    this.LoadTable();
  }
  LoadPermis(){
    this.permisService.loadPermission().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.permis=data[0].Data;
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  LoadUser(){
    this.userService.loadUser().subscribe(
      data => {
        if(data[0].Status=="Complete"){
          this.user=data[0].Data;
          
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  onSave(){
    if(this.username!="" && this.fullname!=""&&this.permisid!="-1" && this.password!=""){
      this.text_alert="";
      this.txt_alertcode="";
      this.txt_alertname="";
	  this.txt_alertpw="";
    this.user_Class.UserName=this.username;
    this.user_Class.FullName=this.fullname;
    this.user_Class.PermisID=Number(this.permisid);
	this.md5 = new Md5();
  //this.user_Class.Password=this.password;
  if(this.old_pass==this.password){
    this.user_Class.Password=this.old_pass;
  }
  else{
    this.user_Class.Password=this.md5.appendStr(this.password).end().toString();
  }
	
	this.user_Class.UserBy=Number('1002');
    if(this.status=="1"){
      this.user_Class.IsActive=true;
    }else{
      this.user_Class.IsActive=false;
    }
    if(this.mode=="add"){
        this.CheckUserCodeDup(this.username);
      }
      else{
		
        this.user_Class.UserID=Number(this.userid);
        this.userService.editUser(this.user_Class).subscribe(
          data => { 
            if(data[0].Status=="Complete"){
                this.LoadUser();
                $('#exampleModal').modal('hide');
				$('#dtUser').DataTable().destroy();
                this.LoadTable();
				this.txtAlert="แก้ไขข้อมูลผู้ใช้งานเรียบร้อยแล้ว";
				$('#modalShow').modal('show');
            }
          },
          err => {
            console.log(err);
          });
      }
      
    }

    if(this.permisid=="-1"){
        this.text_alert="โปรดเลือกกลุ่มสินค้า";
    }
    else{
      this.text_alert="";
    }
    if(this.username==""){
      this.txt_alertcode="โปรดกรอกชื่อบัญชีผู้ใช้งาน";
    }else{
      this.txt_alertcode="";
    }
    if(this.fullname==""){
      this.txt_alertname="โปรดกรอกชื่อผู้ใช้งาน";
    }else{
      this.txt_alertname="";
    }
	if(this.password==""){
      this.txt_alertpw="โปรดกรอกรหัสผ่าน";
    }else{
      this.txt_alertpw="";
    }
  }
  addUser(){
      this.mode="add";
	  this.button="เพิ่มผู้ใช้งาน";
	  this.userid="";
      this.username="";
      this.fullname="";
	  this.password="";
      this.permisid="-1";
      this.status="1";
      this.titlePopup="เพิ่มผู้ใช้งาน"
      this.text_alert="";
	  this.isEdit=false;
	  this.text_alert="";
      this.txt_alertcode="";
      this.txt_alertname="";
	  this.txt_alertpw="";
      console.log(this.mode);
  }
  //item.ProductIX,item.ProductCode,item.ProductName,item.IsActive,item.GroupID
  onEditbtnClick(userid,username,fullname,password,permisid,isactive){
      this.titlePopup="แก้ไขผู้ใช้งาน"
      this.mode="edit";
	  this.button="แก้ไขผู้ใช้งาน";
      this.userid=userid;
      this.username=username;
      this.fullname=fullname;
	  this.password=password;
      this.permisid=permisid;
      this.text_alert="";
	  this.isEdit=true;
	  this.text_alert="";
      this.txt_alertcode="";
      this.txt_alertname="";
    this.txt_alertpw="";
    this.old_pass= password;
      if(isactive){
        this.status="1";
      }else{
        this.status="0";
      }
  }
   CheckUserCodeDup(username:string){
    this.userService.checkDupCode(username).subscribe(
      data => {
        if(data[0].Status=="Complete"){
          if(data[0].Num==0){
            this.userService.addUser(this.user_Class).subscribe(
              data => { 
                if(data[0].Status=="Complete"){
                    this.LoadUser();
                    $('#exampleModal').modal('hide');
					$('#dtUser').DataTable().destroy();
                    this.LoadTable();
					this.txtAlert="เพิ่มผู้ใช้งานเรียบร้อยแล้ว";
					$('#modalShow').modal('show');
                }
              },
              err => {
                console.log(err);
              });
          }
          else{
            this.txt_alertcode="ผู้ใช้งานนี้มีอยู่ในระบบแล้ว";
            
          }
        }
      },
      err => {
        console.log(err);
        alert(err);
      });
  }
  
   LoadTable(){
    
    setTimeout(function () {
        $(function () {
      $('#dtUser').DataTable({
        "columnDefs": [
          { "orderable": false, "targets": 4 }
        ],"order": []
      });
        });
    }, 2000);
  }

}
