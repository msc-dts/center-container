import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductgroupComponent } from './productgroup/productgroup.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import { ProductComponent } from './product/product.component';
import { OrderComponent } from './order/order.component';
import { OrderaddComponent } from './orderadd/orderadd.component';
import { OrdereditComponent } from './orderedit/orderedit.component';
import { UserComponent } from './user/user.component';
import { PermissionComponent } from './permission/permission.component';
import { TableproductionComponent } from './tableproduction/tableproduction.component';
import { CustomerComponent } from './customer/customer.component';
import { LoginComponent } from './login/login.component';
import{  LoginGuardService} from './login-guard.service';
const routes: Routes = [
  {
    path: 'login',
    component:LoginComponent,
    children: []
  },
  {
    path: '',
    component:HomeComponent,
    canActivate:[LoginGuardService],
    children: [
      {
        path:'main',component:MainComponent
      },
      {
        path:'productgroup',component:ProductgroupComponent
      },
      {
        path:'product',component:ProductComponent
      },
      {
        path:'order',component:OrderComponent
      },
      {
        path:'orderadd',component:OrderaddComponent
      },
      {
        path:'orderedit/:orderindex',component:OrdereditComponent
      },
      {
        path:'user',component:UserComponent
      },
      {
        path:'permission',component:PermissionComponent
      },
      {
        path:'tableproduction',component:TableproductionComponent
      },
      {
        path:'customer',component:CustomerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
