/*DataTable Init*/

"use strict"; 

$(document).ready(function() {
	"use strict";
	
	$('#datable_1').DataTable();
	$('#datable_2').DataTable({ "lengthChange": false});

	setTimeout(function () {
        $(function () {
			$('#datable1').DataTable({
				"order": []
			});
        });
	  }, 2000);
	//   setTimeout(function () {
    //     $(function () {
	// 		$('#dtProduct').DataTable();
    //     });
	//   }, 2000);
	setTimeout(function () {
        $(function () {
      $('#dtProduct').DataTable({
        "columnDefs": [
          { "orderable": false,
            "targets": 5
          }
        ],"order": []
      });
        });
    }, 2000);
	  
	  setTimeout(function () {
        $(function () {
			$('#dtOrder').DataTable({
				"footerCallback": function ( row, data, start, end, display ) {
					var api = this.api(), data;
					// Remove the formatting to get integer data for summation
					var intVal = function ( i ) {
							return typeof i === 'string' ?i.replace(/[\$,]/g, '')*1 :typeof i === 'number' ?i : 0;
					};

					// Total over all pages
					var total = api
							.column( 3 )
							.data()
							.reduce( function (a, b) {
									return intVal(a) + intVal(b);
							}, 0 );

					// Total over this page
					var pageTotal = api
							.column( 3, { page: 'current'} )
							.data()
							.reduce( function (a, b) {
									return intVal(a) + intVal(b);
							}, 0 );

					// Update footer
					$( api.column( 3 ).footer() ).html(
						pageTotal +' จากทั้งหมด ('+ total +')'
					);
			}
			});
        });
		}, 1000);

		setTimeout(function () {
			$(function () {
		$('#dtOrder1').DataTable();
			});
	}, 3000);
	  setTimeout(function () {
        $(function () {
			$('#dtOrderItem').DataTable();
        });
      }, 3000);
	
	  setTimeout(function () {
        $(function () {	  
		var table = $('#example').DataTable({
			//'ajax': 'https://api.myjson.com/bins/1us28',  
			'columnDefs': [{
			'targets': 0,
			'searchable':false,
			'orderable':false,
			//'className': 'dt-body-center',
			'render': function (data, type, full, meta){
				return '<input type="checkbox" name="id[]" value="' 
					+ $('<div/>').text(data).html() + '">';
			}
			}],
			'order': [1, 'asc'],
			"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?i.replace(/[\$,]/g, '')*1 :typeof i === 'number' ?i : 0;
            };
 
            // Total over all pages
            var total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            var pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                pageTotal +' จากทั้งหมด ('+ total +')'
            );
        }
		});
  
		// Handle click on "Select all" control
		$('#example-select-all').on('click', function(){
			// Check/uncheck all checkboxes in the table
			var rows = table.rows({ 'search': 'applied' }).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
  
		// Handle click on checkbox to set state of "Select all" control
		$('#example tbody').on('change', 'input[type="checkbox"]', function(){
			// If checkbox is not checked
			if(!this.checked){
			var el = $('#example-select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if(el && el.checked && ('indeterminate' in el)){
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
			}
		});
	  
		});
	}, 2000);

} );